// querySelector trả về elmement đầu tiên khớp
var titleEl = document.querySelector("h5.title");

titleEl.style.color = "red";

var titleList = document.querySelectorAll(".title");

console.log(`  🚀 __ file: index.js:7 __ titleList`, titleList);

titleList[1].style.color = "blue";
// titleList => danh sách
// titleList[1] => 1 phần tử có index = 1 trong danh sách

for (var i = 0; i < titleList.length; i++) {
  titleList[i].style.color = "green";
}
