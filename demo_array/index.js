// pass by value : string, number, boolean
// pass by reference : array, object

var menu = ["bún bò", "bún riu"];

// []
// index : bắt đầu 0, dùng để truy xuất 1 phần tử trong array
console.log(menu[0]);
// update giá trị 1 phần tử trong array
menu[0] = "Bún bò huế";
console.log(`  🚀 __ file: index.js:7 __ menu`, menu);
//  thêm phần tử vào array
menu.push("Hủ tíu");
console.log(`  🚀 __ file: index.js:10 __ menu`, menu);

// crud create read update delete

// tìm kiếm
// vd tìm kiếm index bún riu
// indexOf trả về vị trí ( index ), trả về trừ 1 nếu ko thấy
let indexBunRiu = menu.indexOf("Bún riu");
console.log(`  🚀 __ file: index.js:17 __ indexBunRiu`, indexBunRiu);
menu[indexBunRiu] = "Bánh canh";
console.log(`  🚀 __ file: index.js:19 __ menu`, menu);

// duyệt mảng

// for
var introFood = function (item) {
  console.log("Đây là món: ", item);
};

for (var i = 0; i < menu.length; i++) {
  var food = menu[i];
  introFood(food);
}

// forEach ~ high order function
menu.forEach(function (item, index) {
  console.log("This is: ", item, index);
});
// callback function

// indexOf : tìm vị trí, nếu ko thấy return về -1
var numbers = [30, 50, 70, 30];
// splice( vị trí, số lượng )
numbers.splice(1, 2);
console.log(`  🚀 __ file: index.js:48 __ numbers`, numbers);

var index1 = numbers.indexOf(30);
console.log(`  🚀 __ file: index.js:48 __ index1`, index1);
// mutable and unmutable array method

var color = ["red", "black", "blue"];

var newArray = color.map(function (item) {
  return "alice " + item;
});
// filter :lọc ( ví dụn lọc sản phẩm theo yêu cầu của user)
var newArray2 = color.filter(function (item) {
  return item != "black";
});
console.log(`  🚀 __ file: index.js:63 __ newArray2`, newArray2);

console.log(`  🚀 __ file: index.js:59 __ newArray`, newArray);
